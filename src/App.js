import React, { useState, useEffect } from "react";
import {
  createBrowserRouter,
  Route, RouterProvider, createRoutesFromElements,
} from "react-router-dom";
import Layout from "./screens/layout";
import MainScreen from "./screens/mainScreen/MainScreen";
import CartScreen from "./screens/cartScreen";
import FavoritesScreen from "./screens/favoritesScreen";

import './App.css';

const App = () => {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState([] || localStorage.getItem('cart'));
  const [favorites, setFavorites] = useState([] || localStorage.getItem('favorites'));

  useEffect(() => {
    fetch('products.json')
    .then(res => res.json())
    .then((products) => setProducts(products))
  }, [])

  useEffect(() => {
    localStorage.setItem('favorites', JSON.stringify(favorites));
    localStorage.setItem('cart', JSON.stringify(cart));
  });

  const addToCart = (product) => {
    let isInCart = false;
    cart.forEach((item) => {
      if (item.id === product.id) {
        isInCart = true;
      }
    });

    if (!isInCart) {
      setCart([...cart, product])
    };
  }

  const addToFavorites = (product) => {
    (product.isFavorite) ?
      setFavorites([...favorites, product]) :
        deleteFromFavorites(product)
  }

  const deleteFromFavorites = (product) => {
    setFavorites(favorites.filter((item) => item.id !== product.id) );
  }

  const deleteFromCart = (id) => {
    setCart(cart.filter((item) => item.id !== id) );
  }

  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route element={<Layout cart={cart} favorites={favorites} />}>
        <Route path="/" element={<MainScreen products={products} onAddToCart={addToCart} onAddToFavorites={addToFavorites} />} />
        <Route path="/cart" element={<CartScreen cartItems={cart} onDeleteFromCart={deleteFromCart} />} />
        <Route path="/favorites" element={<FavoritesScreen favorites={favorites} />} />
      </Route>
    )
  );

  return (
    <RouterProvider router={router} products={products} />
  );
}

export default App;