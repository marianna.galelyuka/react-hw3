import React from 'react';
import Product from '../product';
import PropTypes from 'prop-types';

import styles from './ProductList.module.css';

const ProductList = (props) => {
    return (
        <main>
            <h2 className={styles.main__title}>Our products</h2>
            <div className={styles.productList}>
                {props.products.map((el) => (
                    <Product product={el} key={el.id} onAddToCart={props.onAddToCart} onAddToFavorites={props.onAddToFavorites} />
                ))}
            </div>
        </main>
    )
}

ProductList.propTypes = {
    products: PropTypes.array,
    onAddToCart: PropTypes.func,
    onAddToFavorites: PropTypes.func
}

export default ProductList;