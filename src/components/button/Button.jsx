import React from "react";
import PropTypes from "prop-types";

const Button = (props) => {
    return (
        <button className={props.className} style={{backgroundColor: props.backgroundColor}} onClick={props.onClick}>
            {props.text}
        </button>
    )
}

Button.propTypes = {
    onClick: PropTypes.func,
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    className: PropTypes.string,
};

Button.defaultProps = {
    backgroundColor: "rgb(50, 150, 150)",
};

export default Button;