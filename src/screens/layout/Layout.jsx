import React from 'react';
import { Outlet } from 'react-router-dom';
import Header from '../../components/header';

const Layout = (props) => {
	return (
		<>
			<header >
				<Header cart={props.cart} favorites={props.favorites} />
			</header>

			<section>
				<Outlet />
			</section>
		</>
	);
};

export default Layout;
