import React from 'react';
import Item from '../../components/item';

const CartScreen = ({ cartItems, onDeleteFromCart }) => {

    const calculateTotal = (Arr) => {
        let totalSum = 0;
        Arr.forEach((el) => {
            totalSum += Number.parseFloat(el.price)
        })
        return totalSum.toFixed(3)
    }

    return (
        <div>
            <h2>Your cart</h2>
            {cartItems.length ? (
            <div>
                {cartItems.map((el) => (
                    <Item key={el.id} item={el} isCloseIcon={true} onDeleteFromCart={onDeleteFromCart}/>
                ))}
                <b style={{display: "block", textAlign: "center", margin: "48px 0px"}}>
                    Total: UAH {calculateTotal(cartItems)}
                </b>
            </div>) : <p style={{ textAlign: "center" }}>You cart is empty!</p>
            }
        </div>
    );
}

export default CartScreen;